package br.ucsal.testequalidade.bes20181.exemplo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class FatorialUnitarioTest {

	Fatorial fatorial;
	
	@Before
	public void setup(){
		fatorial = new Fatorial();
	}
	
	@Test
	public void calcularFatorial0(){
		Integer n=0;
		Long fatorialEsperado = 1L;
		Long fatorialAtual = fatorial.calcularFatorial(n);
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	public void calcularFatorial1(){
		Integer n=1;
		Long fatorialEsperado = 1L;
		Long fatorialAtual = fatorial.calcularFatorial(n);
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	public void calcularFatorial2(){
		Integer n=2;
		Long fatorialEsperado = 2L;
		Long fatorialAtual = fatorial.calcularFatorial(n);
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}


}
