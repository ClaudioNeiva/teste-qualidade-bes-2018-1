package br.ucsal.testequalidade.bes20181.exemplo;

public class FatorialMock extends Fatorial {

	@Override
	Long calcularFatorial(int n) {
		switch (n) {
		case 0:
			return 1L;
		case 1:
			return 1L;
		case 2:
			return 2L;
		case 3:
			return 6L;
		default:
			return null;
		}
	}

}
